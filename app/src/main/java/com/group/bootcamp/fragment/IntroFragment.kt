package com.group.bootcamp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.group.bootcamp.R

class IntroFragment : Fragment() {
    companion object {
        fun newInstance(): IntroFragment = IntroFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_intro, container, false)
    }

}