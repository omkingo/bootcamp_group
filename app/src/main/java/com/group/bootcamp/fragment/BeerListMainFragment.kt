package com.group.bootcamp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.group.bootcamp.R
import com.group.bootcamp.adapter.BeerListAdapter
import com.group.bootcamp.adapter.BeerListPresenter
import com.group.bootcamp.beer_interface.BeerApiPresenterInterface
import com.group.bootcamp.beer_interface.BeerItemClickListener
import com.group.bootcamp.model.BeerModel
import com.group.bootcamp.service.BeerManager
import com.group.bootcamp.ui.beer.BeerDetailActivity
import kotlinx.android.synthetic.main.beerlist.*

class BeerListMainFragment : Fragment(), BeerApiPresenterInterface {

    companion object {
        // tell that what value should send when navigate
        fun newInstance(): BeerListMainFragment = BeerListMainFragment()
    }

    private val presenter = BeerListPresenter(this, BeerManager().createService())

    override fun setBeer(beerModelList: List<BeerModel>) {

        val listener = object : BeerItemClickListener {
            override fun onItemClick(beerModel: BeerModel) {
                BeerDetailActivity.startActivity(context, beerModel)
            }
        }
        val beerListAdapter = BeerListAdapter(beerModelList, listener)
        rvBeerList.adapter = beerListAdapter
        rvBeerList.layoutManager = LinearLayoutManager(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.beerlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getBeerApi()
    }

}