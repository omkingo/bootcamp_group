package com.group.bootcamp.beer_interface

import com.group.bootcamp.model.BeerModel

interface BeerItemClickListener {

    fun onItemClick(beerModel: BeerModel)

}