package com.group.bootcamp.beer_interface

import com.group.bootcamp.model.BeerModel

interface BeerApiPresenterInterface {

    fun setBeer(beerModelList: List<BeerModel>)

}