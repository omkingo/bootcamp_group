package com.group.bootcamp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.group.bootcamp.model.FragmentModel
import com.group.bootcamp.ui.main.FragmentInterface
import com.group.bootcamp.ui.main.FragmentPresenter
import com.group.bootcamp.ui.main.IntroPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), FragmentInterface {

    companion object {
        const val INTRO_FRAGMENT_TITLE_TAB_NAME = "Introduction"
        const val LIST_BEER_TITLE_TAB_NAME = "Beers"
    }

    private val presenter = FragmentPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.getFragment()
    }


    override fun setFragment(fragmentList: List<FragmentModel>) {
        val sectionsPagerAdapter = IntroPagerAdapter(fragmentList, supportFragmentManager)
        viewPager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(viewPager)
    }

}