package com.group.bootcamp.service

import com.group.bootcamp.model.BeerModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface BeerApiService {

    @GET("v2/beers")
    fun getBeerList(@Query("page") page: Int, @Query("per_page") perPage: Int): Call<List<BeerModel>>

    @GET("v2/beers/{id}")
    fun getBeerById(@Path("id") beerId: Int): Call<List<BeerModel>>

}