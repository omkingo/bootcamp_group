package com.group.bootcamp.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BeerModel(

    @SerializedName("id")
    val id: Int?,

    @SerializedName("name")
    val name: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("abv")
    val abv: Double?,

    @SerializedName("image_url")
    val imageUrl: String?,

    @SerializedName("brewers_tips")
    val brewersTips: String?,

    @SerializedName("first_brewed")
    val firstBrewed: String?

) : Parcelable