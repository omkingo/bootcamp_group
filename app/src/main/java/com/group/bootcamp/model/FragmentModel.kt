package com.group.bootcamp.model

import androidx.fragment.app.Fragment

class FragmentModel(
    val tabName: String,
    val fragment: Fragment
)