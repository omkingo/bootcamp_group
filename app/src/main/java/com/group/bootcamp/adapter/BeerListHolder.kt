package com.group.bootcamp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.group.bootcamp.R
import com.group.bootcamp.beer_interface.BeerItemClickListener
import com.group.bootcamp.model.BeerModel
import com.group.bootcamp.ui.showImage

class BeerListHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.beerlist_card, parent, false)
) {

    private val beerPic: ImageView = itemView.findViewById(R.id.beerPic)
    private val beerName: TextView = itemView.findViewById(R.id.beerName)

    fun bind(model: BeerModel, listener: BeerItemClickListener) {

        itemView.setOnClickListener {
            listener.onItemClick(model)
        }

        beerPic.showImage(model.imageUrl)
        beerName.text = model.name
    }

}
