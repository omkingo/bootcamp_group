package com.group.bootcamp.adapter

import com.group.bootcamp.beer_interface.BeerApiPresenterInterface
import com.group.bootcamp.model.BeerModel
import com.group.bootcamp.service.BeerApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BeerListPresenter(val view: BeerApiPresenterInterface, private val service: BeerApiService) {

    companion object {
        const val PAGE: Int = 1
        const val PER_PAGE: Int = 25
    }

    fun getBeerApi() {
        service.getBeerList(PAGE, PER_PAGE).enqueue(object : Callback<List<BeerModel>> {
            override fun onFailure(call: Call<List<BeerModel>>, t: Throwable) { }

            override fun onResponse(call: Call<List<BeerModel>>, response: Response<List<BeerModel>>) {
                response.body()?.apply {
                    if (this.isNotEmpty()) {
                        view.setBeer(this)
                    }
                }
            }

        })
    }

}