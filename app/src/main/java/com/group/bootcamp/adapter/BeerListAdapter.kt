package com.group.bootcamp.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.group.bootcamp.beer_interface.BeerItemClickListener
import com.group.bootcamp.model.BeerModel

class BeerListAdapter(
    private val beerList: List<BeerModel>,
    private val listener: BeerItemClickListener
) : RecyclerView.Adapter<BeerListHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerListHolder {
        return BeerListHolder(parent)
    }

    override fun getItemCount(): Int = beerList.count()

    override fun onBindViewHolder(holder: BeerListHolder, position: Int) {
        holder.bind(beerList[position], listener)
    }

}