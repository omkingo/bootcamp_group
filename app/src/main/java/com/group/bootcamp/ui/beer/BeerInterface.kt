package com.group.bootcamp.ui.beer

interface BeerInterface {
    fun setBeer(
        beerName: String,
        beerDes: String,
        firstBrew: String,
        tips: String,
        imageUrl: String?
    )
}