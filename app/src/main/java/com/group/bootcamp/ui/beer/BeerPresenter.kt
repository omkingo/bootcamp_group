package com.group.bootcamp.ui.beer

import com.group.bootcamp.model.BeerModel

class BeerPresenter(private val view: BeerInterface) {

    fun checkBeer(beerItem: BeerModel) {
        view.setBeer(
            "${beerItem.name} (${beerItem.abv ?: 0.0})",
            beerItem.description ?: "",
            beerItem.firstBrewed ?: "",
            beerItem.brewersTips ?: "",
            beerItem.imageUrl
        )
    }

}
