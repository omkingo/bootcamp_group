package com.group.bootcamp.ui.main

import com.group.bootcamp.MainActivity
import com.group.bootcamp.fragment.BeerListMainFragment
import com.group.bootcamp.fragment.IntroFragment
import com.group.bootcamp.model.FragmentModel

class FragmentPresenter(private val view: FragmentInterface) {

    fun getFragment() {
        val tabList = listOf(
            FragmentModel(
                MainActivity.INTRO_FRAGMENT_TITLE_TAB_NAME,
                IntroFragment.newInstance()
            ),
            FragmentModel(
                MainActivity.LIST_BEER_TITLE_TAB_NAME,
                BeerListMainFragment.newInstance()
            )
        )

        view.setFragment(tabList)
    }


}