package com.group.bootcamp.ui.beer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.group.bootcamp.R
import com.group.bootcamp.model.BeerModel
import com.group.bootcamp.ui.showImage
import kotlinx.android.synthetic.main.activity_beer_detail.*

class BeerDetailActivity : AppCompatActivity(), BeerInterface {

    companion object {
        const val EXTRA_KEY_MODEL = "MODEL"

        fun startActivity(context: Context?, model: BeerModel) =
            context?.startActivity(
                Intent(context, BeerDetailActivity::class.java).also { intent ->
                    intent.putExtra(EXTRA_KEY_MODEL, model)
                }
            )
    }

    private val presenter = BeerPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer_detail)
        presenter.checkBeer(intent.getParcelableExtra(EXTRA_KEY_MODEL))
    }

    override fun setBeer(
        beerName: String,
        beerDes: String,
        firstBrew: String,
        tips: String,
        imageUrl: String?
    ) {
        tvName.text = beerName
        tvDesc.text = beerDes
        tvfirstBrewed.text = firstBrew
        tvBrewersTips.text = tips
        imgBeer.showImage(imageUrl)
    }
}