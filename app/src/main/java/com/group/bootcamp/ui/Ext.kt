package com.group.bootcamp.ui

import android.widget.ImageView
import com.group.bootcamp.R
import com.squareup.picasso.Picasso

fun ImageView.showImage(imageUrl: String?) {
    Picasso.get()
        .load(imageUrl.validUrl())
        .placeholder(R.mipmap.ic_launcher_beer)
        .into(this)
}

fun String?.validUrl(): String? {
    return this?.let {
        if (this.isEmpty()) {
            null
        } else {
            this
        }
    }
}