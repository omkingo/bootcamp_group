package com.group.bootcamp.ui.main

import com.group.bootcamp.model.FragmentModel

interface FragmentInterface {
    fun setFragment(fragmentList: List<FragmentModel>)
}